# 선희수 CRUD 포트폴리오(1)

## 2023-01-08

- first 커밋
- Login UI 구성(디자인 X)
- 회원 가입 라우팅 추가
- javascript 외부 모듈 스니펫을 위한 npm init 및 @type/jquery 추가

## 2023-01-09

- Login UI 간단한 시각적인 디자인 요소 추가
- 회원 가입 UI 기본 구성
- javascript(jquery) 외부 모듈로 사용할건지 검토 필요

## 2023-01-10

- 회원 가입을 위한 Oracle DB 연동 @Test 완료
  <!-- @slf4j 어노테이션을 선언하면 아래와 같이 Logger 객체 생성이 필요 없다. -->
  <!-- private final Logger log = LoggerFactory.getLogger(getClass()); -->

## 2023-01-11

- logger 공부
- 회원 가입을 위한 Oracle DB 연동  (예정)
  - logger(slf4j, log4j, log4j2 등 공부 후)

## 2023-01-16

- Oracle DB 연동 완료
- Controller 에서 회원 가입 로직을 테스트를 위해 로그인 구현 로직으로 대체
- DB 정보 조회, 삽입 기능 40% 숙달

## 2023-01-17  (퇴근 후 회식 및 익일 출장인 관계로 이른 종료)

- 로그인 로직에서 회원가입 로직으로 변경
- 아이디 생성 규칙 적용(8자 이상, 20자 이하)
- 암호, 재확인 암호 일치 여부 구현
- 생성 규칙에 어긋날 경우 웹에 alert 띄우는 코드 적용  (예정)

## 2023-02-12

- 로그인 후 세션 저장 구현
- 세션 유지에 따른 컨텐츠 뷰 요청 처리  (예정)
