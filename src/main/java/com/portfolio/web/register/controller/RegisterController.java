package com.portfolio.web.register.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.portfolio.web.register.service.RegisterService;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@Controller
public class RegisterController {
    @Autowired
    RegisterService registerService;

    @GetMapping(path = "/register")
    public String getRegister(HttpServletRequest request, Model model) {
        return registerService.getRegister(request, model);
    }

    /**
     * 회원가입 로직
     */
    @PostMapping("/register")
    public String postRegister(HttpServletRequest request, HttpServletResponse response, Model model) {
        return registerService.postRegister(request, response, model);
    }
}
