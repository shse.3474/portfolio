package com.portfolio.web.register.service;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import com.portfolio.web.common.Accounts;
import com.portfolio.web.common.AccountsReposit;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@Service
public class RegisterService {
    public String getRegister(HttpServletRequest request, Model model) {
        return "register";
    }

    /**
     * 회원가입 로직
     */
    @Autowired
    AccountsReposit accountsReposit;

    public String postRegister(HttpServletRequest request, HttpServletResponse response, Model model) {
        Accounts accounts = new Accounts();

        String name = request.getParameter("name");
        String id = request.getParameter("id");
        String pw = request.getParameter("pw");
        String rePw = request.getParameter("re-pw");
        String email = request.getParameter("email");
        LocalDateTime date = LocalDateTime.now();

        List<Accounts> selectEmail = accountsReposit.findByEmail(request.getParameter("email"));
        List<Accounts> selectId = accountsReposit.findById(request.getParameter("id"));

        try {
            System.out.println("###############  REGISTER STATUS:: TRY  ###############");
            if (pw.equals(rePw) && id.length() >= 8 && id.length() <= 20 && selectEmail.isEmpty()) { // 회원 가입 성공 시
                accounts.setName(name);
                accounts.setId(id);
                accounts.setPw(pw);
                accounts.setEmail(email);
                accounts.setCreateDate(date);

                this.accountsReposit.save(accounts);

                model.addAttribute("status", "1");

                response.sendRedirect("/");

                System.out.println("###############  REGISTER STATUS:: OK  ###############");
            } else if (!pw.equals(rePw)) { // 암호, 재입력 암호 부분 불일치 경우
                System.out.println("암호와 암호 재입력 부분이 일치하지 않습니다.");
                model.addAttribute("status", "2");
            } else if (id.length() <= 7 || id.length() > 21) { // 생성 규칙에 맞지 않을 경우
                System.out.println("ID 최소 규칙은 8자 이상, 20자 이하입니다.\n생성 규칙에 맞게 다시 진행해주세요.");
            }
        } catch (DataIntegrityViolationException dive) { // 계정 중복 에러 발생 시
            if (!selectEmail.isEmpty()) { // 이미 이메일이 존재할 경우
                model.addAttribute("status", "3");
                System.out.println("이메일 중복 에러");
            } else if (!selectId.isEmpty()) { // 이미 아이디가 존재할 경우
                model.addAttribute("status", "4");
                System.out.println("아이디 중복 에러");
            }
            System.out.println("중복 ERROR: " + dive);
        } catch (Exception e) {
            System.out.println("알 수 없는 ERROR 발생: " + e);
        }
        return "register";
    }
}
