package com.portfolio.web.main.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import com.portfolio.web.common.service.SessionService;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;

@Service
public class MainService {

    @Autowired
    SessionService sessionService;

    public String getMain(HttpServletRequest request, HttpSession session, Model model) {
        sessionService.expireSession(session, model);
        model.addAttribute("status", SessionService.sessionId);
        model.addAttribute("sessionId", session.getId());
        return "main/main";
    }
}
