package com.portfolio.web.common;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

// JpaRepository 상속 시 제네릭스 타입으로 리포짓 대상 엔티티와 PK 속성을 지정
public interface AccountsReposit extends JpaRepository<Accounts, Integer> {

    List<Accounts> findById(String id);

    List<Accounts> findByPw(String pw);

    List<Accounts> findByEmail(String email);

    Accounts findByIdx(int idx);
}
