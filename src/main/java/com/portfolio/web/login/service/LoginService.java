package com.portfolio.web.login.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import com.portfolio.web.common.Accounts;
import com.portfolio.web.common.AccountsReposit;
import com.portfolio.web.common.service.SessionService;

import jakarta.persistence.NonUniqueResultException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;

@Service
public class LoginService {

    @Autowired
    AccountsReposit accountsReposit;

    @Autowired
    SessionService sessionService;

    public String getLogin() {
        return "login";
    }

    public void tryLogin(HttpServletRequest request, Model model, HttpSession session) {

        /* 쿼리 데이터 조회 (JPA) */
        List<Accounts> selectId = accountsReposit.findById(request.getParameter("id"));
        List<Accounts> selectPw = accountsReposit.findByPw(request.getParameter("pw"));

        System.out.println("===============");
        System.out.println("\"" + request.getParameter("id") + "\"" + " 계정으로 로그인 시도");
        System.out.println("===============");

        try {
            if (!selectId.isEmpty() && !selectPw.isEmpty()) {
                SessionService.sessionId = session.getId(); // 브라우저 세션 값을 서버측에 저장

                session.setMaxInactiveInterval(600); // 세션 유지시간 60초

                System.out.println("JSESSIONID2: " + SessionService.sessionId);
                System.out.println("LOGIN STATUS::  OK");

                model.addAttribute("loginStatus", "1");
                model.addAttribute("msg_1", "로그인에 성공하였습니다.");
            } else if (selectId.isEmpty() || selectPw.isEmpty()) {
                System.out.println("LOGIN STATUS::  FAIL: 존재하지 않는 계정으로 접속 시도");

                model.addAttribute("loginStatus", "2");
                model.addAttribute("msg_2", "존재하지 않는 계정입니다.\\n\\nID 혹은 PW를 확인해주세요.");
            }
        } catch (NullPointerException ne) { // 일치하는 계정 정보를 찾을 수 없을 때
            if (selectId == null) {
                System.out.println("일치하는 ID를 찾을 수 없습니다.\n" + ne);
            } else if (selectPw == null) {
                System.out.println("일치하는 암호를 찾을 수 없습니다.\n" + ne);
            }
        } catch (ClassCastException cce) {
            System.out.println("알 수 없는 ERROR 발생: " + cce);
        } catch (NonUniqueResultException nure) {
            System.out.println("알 수 없는 ERROR 발생: " + nure);
        } catch (IncorrectResultSizeDataAccessException irsdae) {
            System.out.println("알 수 없는 ERROR 발생: " + irsdae);
        } catch (Exception e) {
            System.out.println("알 수 없는 ERROR 발생: " + e);
        }
    }
}
