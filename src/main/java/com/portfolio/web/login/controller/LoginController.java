package com.portfolio.web.login.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.portfolio.web.login.service.LoginService;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;

@Controller
public class LoginController {
    @Autowired
    LoginService loginService;

    @GetMapping("/login")
    public void getLogin() {
        loginService.getLogin();
    }

    @PostMapping("/login")
    public void tryLogin(HttpServletRequest request, Model model, HttpSession session) {
        loginService.tryLogin(request, model, session);
    }
}
