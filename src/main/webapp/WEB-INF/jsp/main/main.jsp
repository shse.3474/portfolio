<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>


<!DOCTYPE html>
<html lang="ko">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>main</title>

        <link rel="stylesheet" href="/css/mainStyle.css">

        <script src="https://code.jquery.com/jquery-latest.min.js"></script>
    </head>

<body>
        <header>
            <div id="border">
                <div>인기물</div>
                <div>갤러리</div>
                <div>"${sessionId}"</div>  <!-- MainService 에서 넘겨받은 세션 값 -->
                <div>"${status}"</div>
            </div>
        </header>

        <div id="main">
            <hr>
            <div id="pop">
                <div class="pops" id="pop1">
                    <img src="${url}" alt="">
                </div>
                <div class="pops" id="pop2"></div>
                <div class="pops" id="pop3"></div>
                <div class="pops" id="pop4"></div>
            </div>
        </div>
        <footer></footer>
    </body>


</html>
