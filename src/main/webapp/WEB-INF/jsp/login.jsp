<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>


<!DOCTYPE html>
<html lang="ko">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>login</title>

        <link rel="stylesheet" href="/css/loginStyle.css">

        <script src="https://code.jquery.com/jquery-latest.min.js"></script>
    </head>

    <body>
        <div id="tempMainPhrase">First Portfolio</div>
        <div id="tempLoginForm">
            <div id="tempLoginForm2">
                <form id="tempInputAccount" action="/login" method="post">
                    <input class="tempInput" type="text" placeholder="ID" name="id">
                    <br><br>
                    <input class="tempInput" type="password" placeholder="PW" name="pw">
                    <br><br>
                    <input id="tempLogin" class="tempInput" type="submit" value="로그인">
                    <div>${loginStatus}</div>
                </form>
                <br><br>
                <div id="tempRegistration">
                    <div id="tempRegistrationGuide">포트폴리오를 구경하고 싶으시다면?? 아이디를 생성하세요!</div>
                    <br>
                    <input id="tempGoRegister" type="button" value="회원 가입">
                    <!-- 회원 가입 버튼 클릭 시 이동 -->
                </div>
            </div>
        </div>
    </body>
    <script>
        var loginStatus = "${loginStatus}";
        var msg_1 = "${msg_1}";  // 성공
        var msg_2 = "${msg_2}";  // 실패

        if (loginStatus == 1) {
            alert(msg_1);
            location.href="/";
        } else if (loginStatus == 2) {
            alert(msg_2);
            // location.href = "/register";
        }


        document.getElementById("tempGoRegister").addEventListener("click", () => {
            location.href = "/register";
        });
    </script>

</html>
